# frozen_string_literal: false
# require_relative 'person'

module Pivot
  class Item
    # Code here!

    attr_accessor :name, :assignee, :points, :project_code
    def initialize(name, assignee, points)
        @name = name
        @assignee = assignee
        @points = points
                   
    end

    def name
      @name
    end
    def assignee
      @assignee
    end
    def points
      @points
    end



    def project_code
      @name.gsub!(/[0-9]/, '')
      @name.gsub!(/\W/, '')
    end

    def +
      sum = item.points.to_i + another_item.points.to_i
    end

    def valid?
      if project_code == 'EREC' || project_code == 'AZR'
        # self.be_valid = true
        true
      else
        # elsif project_code != 'EREC' || project_code != 'AZR'
        # elsif project_code == 'CHOOCHOO'
        # self.be_valid = false
        false
      end
    end
  end

end

  # person = Pivot::Person.new('joe@example', 'joe','nakar' )
  # item = Pivot::Item.new("EREC-10", person.email, 3)
  # item1 = Pivot::Item.new("EREC-11", person.email, 4)
  # item2 = Pivot::Item.new("EREC-12", person.email, 5)



  
