# frozen_string_literal: true

module Pivot
  class Tracker
    # Code here!
    require_relative 'item.rb'
    attr_accessor :items

    def initialize(items)
      @items = items
    end

    def count(_items)
      count
    end
  end
end

# items = [
#       Pivot::Item.new('EREC-10', 'smith@example.com', 2),
#       Pivot::Item.new('EREC-21', 'johndough@example.com', 4),
#       Pivot::Item.new('EREC-32', 'cesar@example.com', 3),
#       Pivot::Item.new('EREC-11', 'bruno@example.com', 5),
#       Pivot::Item.new('EREC-12', 'cesar@example.com', 3),
#       Pivot::Item.new('EREC-13', 'genericman@example.com', 2),
#       Pivot::Item.new('EREC-14', 'johndough@example.com', 1)
#     ]

# puts items.count

# puts Pivot::Tracker.count(items)
